## Location Service Centre ##

This application is designed to track the movement of member of staff through his daily routine.

QR Codes are placed at designated points and the employee scans them from the mobile app as he moves through his daily routine.

This repository conatins the admin web app and API's. 

---

## Architecture

The app is build in Java(8) and designed to run on Google App Engine.

User authentication is handled by the Google Gsuit user access API's in the google cloud.

Access is limited by Domain space. Only a single domain is currently supported.

Databases are Google Cloud SQL instances running MySql.

