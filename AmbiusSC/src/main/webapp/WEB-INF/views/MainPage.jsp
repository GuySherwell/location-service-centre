<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Ambius SC</title>
</head>
<body style="padding-top:65px">
	<div class="container-fluid">
		<nav class="navbar navbar-inverse navbar-fixed-top">
			<div class="container-fluid">
				<div class="navbar-header">
					<a class="navbar-brand" href="/hello" style="color:green;"><span class="glyphicon glyphicon-home"></span>  Ambius SC</a>
				</div>
				<ul class="nav navbar-nav">
					<li class="active"></li>
				</ul>
			</div>
		</nav>
		<div class="container">
			<h1>Welcome to Ambius Service Center</h1>
			<table class="table" style="text-align:center;">
				<tr>
					<td width = "25%"><pre><a href="/clients"><img src="https://storage.googleapis.com/third-framing-94218.appspot.com/icon_ambius_leafs.png"></a>
Ambius SC Client Config</pre></td>
					<td width = "25%"><pre><a href="https://datastudio.google.com/open/16pI7KmG0mqYyugEr-HuDX9A-ifH6vQHJ" target="_blank"><img src="https://storage.googleapis.com/third-framing-94218.appspot.com/icon_ambius_leafs.png"></a>
Ambius SC Dashboard</pre></td>
					<td width = "25%"></td>
					<td width = "25%"></td>
				</tr>
				<tr>
					
				</tr>
			</table>
		</div>
	</div>
</body>
</html>