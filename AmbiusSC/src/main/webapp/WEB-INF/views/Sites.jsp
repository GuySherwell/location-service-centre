<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Ambius SC</title>
<!-- Bootstrap -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<script>
	$(document).ready(function() {
		$("#addNew").hide();
		$("#update").hide();
		$("#editSiteIdRow").hide();
		$("#editSite").hide();
		$("#deleteSiteId").hide();
		$("#deleteSiteCode").hide();
		$("#clientId").hide();
		$("#editClientId").hide();
		$("#clientName").hide();
		$("#editClientName").hide();
		$("#warning").hide();
		$("#add").hide();
		$("#dup").hide();
		$("#editDup").hide();
		$('#name').on('keyup', function() {
			validate();
		});
		$('#description').on('keyup', function() {
			validate();
		});
		$('#code').on('keyup', function() {
			validate();
		});
		$('#editName').on('keyup', function() {
			validateUpdate();
		});
		$('#editDescription').on('keyup', function() {
			validateUpdate();
		});
		$('#editCode').on('keyup', function() {
			validateUpdate();
		});
		console.log("Done");

	});
</script>

<style>
.whole {
	position: fixed;
	margin: 0;
	top: 0;
	left: 0;
	width: 100%;
	height: 100%;
	background: rgba(0, 0, 0, 0.2);
}

.add_btn {
	cursor: pointer;
	border-radius: 5px;
	height: 30px;
	width: 100px;
	border: none;
	margin-right: 25px;
	margin-top: 50px;
	background-color: green;
}
</style>
</head>

<body style="padding-top: 65px">
	<div class="container-fluid">
		<nav class="navbar navbar-inverse navbar-fixed-top">
		<div class="container-fluid">
			<div class="navbar-header">
				<a class="navbar-brand" href="/hello" style="color: green;"><span
					class="glyphicon glyphicon-home"></span> Ambius SC</a>
			</div>
			<ul class="nav navbar-nav">
				<li class="active"></li>
			</ul>
		</div>
		</nav>
		<div class="container">
			<h1 style="color: green;">
				<c:out value="${clientName}" />
				Service Sites
			</h1>
			<table class="table table-hover" style="text-align: center;">
				<tr>
					<th>Name</th>
					<th>Description</th>
					<th>Code</th>
					<th>Service Interval</th>
					<th>service Start Date</th>
				</tr>
				<c:forEach items="${siteData}" var="site">
					<tr>
						<td><c:out value="${site.name}" /></td>
						<td><c:out value="${site.description}" /></td>
						<td><c:out value="${site.code}" /></td>
						<td><c:out value="${site.serviceInterval}" /></td>
						<td><c:out value="${site.serviceStartDate}" /></td>

						<td><a href="#"
							onclick='javascript:showEditDialog("<c:out value = "${site.name}" />", "<c:out value = "${site.description}" />", "<c:out value = "${site.code}" />", "<c:out value = "${site.serviceInterval}" />", "<c:out value = "${site.serviceStartDate}" />", "<c:out value = "${site.siteId}" />")'><span
								class='glyphicon glyphicon-pencil'></span></a></td>
					</tr>
				</c:forEach>
			</table>
			<h2>
				<a href="#" onclick='javascript:showNewDialog()'><span
					class="glyphicon glyphicon-plus"></span></a>
			</h2>
		</div>
	</div>
	<div class="whole" id="addNew" style="color: white; padding: 10px;">
		<div
			style="width: 80%; padding-left: 50px; padding-top: 10px; margin: 0 auto; background-color: #9b9b9b; margin-top: 65px;">
			<form action="/addSite" method="POST">
				<h1>Add New Service Site:</h1>
				<table>
					<tr id="clientId">
						<td width="200px">Client Id:</td>
						<td width="100px"><input type="text" name="clientId"
							style="color: black; margin-top: 20px"
							value="<c:out value="${clientId}" />"></td>
					</tr>
					<tr id="clientName">
						<td width="200px">Client Name:</td>
						<td width="100px"><input type="text" name="clientNam"
							style="color: black; margin-top: 20px"
							value="<c:out value="${clientName}" />"></td>
					</tr>
					<tr>
						<td width="200px">Name:</td>
						<td width="100px"><input id="name" type="text" name="name"
							style="color: black; margin-top: 20px"></td>
					</tr>
					<tr style="margin-top: 10px;">
						<td width="200px">Description:</td>
						<td width="500px"><input id="description" type="text"
							name="description"
							style="color: black; width: 100%; margin-top: 20px"></td>
					</tr>
					<tr style="margin-top: 10px;">
						<td width="200px">Code:</td>
						<td width="100px"><input type="text" id="code" name="code"
							style="color: black; margin-top: 20px"></td>
						<td id="dup" style="color: red">* Duplicate Code</td>
					</tr>
					<tr style="margin-top: 10px;">
						<td width="200px">Service Interval:</td>
						<td width="100px"><input type="text" name="serviceInterval"
							style="color: black; margin-top: 20px"></td>
					</tr>
					<tr style="margin-top: 10px;">
						<td width="200px">Service Start Date:</td>
						<td width="100px"><input type="text" name="serviceStartDate"
							style="color: black; margin-top: 20px"></td>
					</tr>
				</table>
				<button type="submit" id="add" class="add_btn">Add</button>
			</form>
			<button onclick="javascript:hideNewDialog()"
				style="border-radius: 5px; height: 30px; width: 100px; border: none; margin-right: 25px; margin-top: 10px; margin-bottom: 20px; background-color: green;">Cancel</button>
		</div>
	</div>
	<div class="whole" id="editSite" style="color: white; padding: 10px;">
		<div id="editSiteForm"
			style="width: 80%; padding-left: 50px; padding-top: 10px; margin: 0 auto; background-color: #9b9b9b; margin-top: 65px;">
			<form action="/updateSite" method="POST">
				<h1>Edit Service Site:</h1>
				<table>
					<tr id="editClientId">
						<td width="200px">Client Id:</td>
						<td width="100px"><input type="text" name="editCientId"
							style="color: black; margin-top: 20px"
							value="<c:out value="${clientId}" />"></td>
					</tr>
					<tr id="editClientName">
						<td width="200px">Client Name:</td>
						<td width="100px"><input type="text" name="editClientNam"
							style="color: black; margin-top: 20px"
							value="<c:out value="${clientName}" />"></td>
					</tr>
					<tr id="editSiteIdRow">
						<td width="200px">Site Id:</td>
						<td width="100px"><input type="text" id="editSiteId"
							name="editSiteId" style="color: black; margin-top: 20px"></td>
					</tr>
					<tr>
						<td width="200px">Name:</td>
						<td width="100px"><input id="editName" type="text"
							name="editName" style="color: black; margin-top: 20px" value=e()></td>
					</tr>
					<tr style="margin-top: 10px;">
						<td width="200px">Description:</td>
						<td width="500px"><input id="editDescription" type="text"
							name="editDescription"
							style="color: black; width: 100%; margin-top: 20px"></td>
					</tr>
					<tr style="margin-top: 10px;">
						<td width="200px">Code:</td>
						<td width="100px"><input type="text" id="editCode"
							name="editCode" style="color: black; margin-top: 20px"></td>
						<td id="editDup" style="color: red">* Duplicate Code</td>
					</tr>
					<tr style="margin-top: 10px;">
						<td width="200px">Service Interval:</td>
						<td width="100px"><input type="text" id="editServiceInterval"
							name="editServiceInterval" style="color: black; margin-top: 20px"></td>
					</tr>
					<tr style="margin-top: 10px;">
						<td width="200px">Service Start Date:</td>
						<td width="100px"><input type="text"
							id="editServiceStartDate" name="editServiceStartDate"
							style="color: black; margin-top: 20px"></td>
					</tr>
				</table>
				<button type="submit" id="update" class="add_btn">Update</button>
			</form>
			<button onclick="javascript:showDeleteWarning()" id="delete_btn"
				class="add_btn">Delete</button>
			<button onclick="javascript:hideEditDialog()"
				style="border-radius: 5px; height: 30px; width: 100px; border: none; margin-right: 25px; margin-top: 10px; margin-bottom: 20px; background-color: green;">Cancel</button>
		</div>
		<div id="warning"
			style="width: 80%; padding-left: 50px; padding-top: 10px; margin: 0 auto; background-color: #9b9b9b; margin-top: 65px; color: red;">
			<h2>Warning</h2>
			<h3>You are about to delete a service site.</h3>
			<h3>This action cannot be undone and the QR Codes in the field
				for this site will no longer function</h3>
			<h3>Are you sure you want to continue?</h3>

			<form action="/deleteSite" method="POST">
				<div style="display: none;">
					<input name="deleteClientName" id="deleteClientName">
					<c:out value="${clientName}" />
					<input name="deleteClientId" id="deleteClientId" />
					<c:out value="${clientId}" />
					<input name="deleteSiteId" id="deleteSiteId" /> <input
						name="deleteSiteCode" id="deleteSiteCode" />
				</div>
				<button type="submit" id="update" class="add_btn">Delete</button>
			</form>
			<button onclick="javascript:hideEditDialog()"
				style="border-radius: 5px; height: 30px; width: 100px; border: none; margin-right: 25px; margin-top: 10px; margin-bottom: 20px; background-color: green;">Cancel</button>
		</div>
	</div>
	<script>
function showNewDialog() {
	$("#addNew").show();
}
function hideNewDialog() {
	$("#addNew").hide();
}
function showEditDialog(name, description, code, interval, start, siteId) {
	console.log(name + " " + description + " " + code + " " + interval
			+ " " + start + " " + siteId);
	$("#editSiteId").val(siteId);
	$("#deleteSiteId").val(siteId);
	$("#deleteSiteCode").val(code);
	$("#editName").val(name);
	$("#editDescription").val(description);
	$("#editCode").val(code);
	$("#editServiceInterval").val(interval);
	$("#editServiceStartDate").val(start);
	console.log("DeleteId= " + deleteSiteId);
	$("#editSite").show();
}
function hideEditDialog() {
	$('#editSiteForm').show();
	$('#warning').hide();
	$("#editSite").hide();
}

function validate() {

	var name = $("#name").val();
	var description = $("#description").val();
	var code = $("#code").val();
	var uniqueCode = false;

	var count = 0;
	var codes = [];
	<c:forEach items="${siteData}" var="site">
	{
		codes[count] = "${site.code}";
		count++;
	}
	</c:forEach>
	console.log("codes: " + codes);

	for (var i = 0; i < codes.length; i++) {
		console.log(code + " VS " + codes[i]);
		if (code === codes[i]) {
			$("#dup").show();
			$("#add").hide();
			break;

		} else {
			if ((name !== "") && (description !== "") && (code !== "")) {
				$("#dup").hide();
				$("#add").show();
			}
		}
	}
}
function validateUpdate() {

	var name = e($("#editName").val());
	var description = e($("#editDescription").val());
	var code = e($("#editCode").val());
	var existingCode = $("#deleteSiteCode").val();
	var uniqueCode = true;

	var count = 0;
	var codes = [];
	<c:forEach items="${siteData}" var="site">
	{
		codes[count] = "${site.code}";
		count++;
	}
	</c:forEach>
	console.log("codes: " + codes);

	for (var i = 0; i < codes.length; i++) {
		console.log(code + " VS " + existingCode);
		if ((code === codes[i]) && (code !== existingCode)) {
			$("#editDup").show();
			console.log("Duplicate code Match: Unique = " + uniqueCode);
			uniqueCode = false;
			break;
		} else {
			uniqueCode = true;
			$("#editDup").hide();
		}
	}
	console.log("Testing with: Unique = " + uniqueCode);
	if ((name !== "") && (description !== "") && (code !== "")
			&& (uniqueCode !== false)) {
		$("#update").show();
	} else {
		$("#update").hide();
	}

}
function showDeleteWarning() {
	$('#editSiteForm').hide();
	$('#warning').show();
}

function e(s) {
	
    return s.replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/"/g, '&quot;');
}


</script>
</body>
</html>
