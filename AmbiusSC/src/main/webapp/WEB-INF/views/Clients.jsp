<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Ambius SC</title>
<!-- Bootstrap -->
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">

<!-- jQuery library -->
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

<!-- Latest compiled JavaScript -->
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">

	<script>
	$(document).ready(function(){
		$("#addNew").hide();
		console.log("Done");
	});
	</script>
<style>
	.whole{
    position:fixed;
    
	margin:0;
    top:0;
    left:0;

    width: 100%;
    height: 100%;
    background:rgba(0, 0, 0, 0.2);
}
</style>
</head>

<body style="padding-top:65px">
	<div class="container-fluid">
		<nav class="navbar navbar-inverse navbar-fixed-top">
			<div class="container-fluid">
				<div class="navbar-header">
					<a class="navbar-brand" href="/hello" style="color:green;"><span class="glyphicon glyphicon-home"></span>  Ambius SC</a>
				</div>
				<ul class="nav navbar-nav">
					<li class="active"></li>
					<!-- 
					<li><a href="#">Page 1</a></li>
					<li><a href="#">Page 2</a></li>
					<li><a href="#">Page 3</a></li>
					-->
				</ul>
			</div>
		</nav>
		<div class="container">
			<h1 style="color:green;">Clients</h1>
			<table class="table table-hover" style="text-align:center;">
				<tr>
					<th>Name</th>
					<th>Address</th>
				</tr>
				<c:forEach items="${ClientData}" var="client">
				<tr>
					<td><a href="/sites?clientId=${client.id}&clientName=${client.name}">${client.name}</a></td>
					<td>${client.address}</td>
					<td><span class="glyphicon glyphicon-pencil"></span></td>
				</tr>
				</c:forEach>
			</table>
			<h2 ><a href="#" onclick='javascript:showNewDialog()'><span class="glyphicon glyphicon-plus"></span></a></h2>
		</div>
	</div>
	<div class="whole" id="addNew" style="color:white; padding:10px;">
		<div style="width:80%; padding-left:50px; padding-top:10px; margin:0 auto; background-color: #9b9b9b; margin-top:65px;">
			<form action="/addClient">
				<h1>Add New Client:</h1>
				<table>
					<tr >
						<td width="100px">Name:</td><td width="100px"><input type="text" name="name" style="color:black; margin-bottom:20px; margin-top:20px" ></td>
					</tr>
					<tr style="margin-top:10px;">
						<td width="100px">Address:</td><td width="500px"><input type="text" name="address" style="color:black;  width:100%" ></td>
					</tr>
				</table>
				<button type="submit" style="border-radius:5px; height:30px; width:100px; border:none; margin-right:25px; margin-top:50px; background-color:green;" >Add</button> 	
			</form>
			<button onclick="javascript:hideNewDialog()" style="border-radius:5px; height:30px; width:100px; border:none; margin-right:25px; margin-top:10px; margin-bottom:20px; background-color:green;">Cancel</button>
		</div>
	</div>
</body>
<script>
	function showNewDialog(){
		$("#addNew").show();
	}
	function hideNewDialog(){
		$("#addNew").hide();
	}
</script>
</html>
