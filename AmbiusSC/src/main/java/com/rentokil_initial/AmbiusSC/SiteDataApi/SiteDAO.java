package com.rentokil_initial.AmbiusSC.SiteDataApi;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import com.rentokil_initial.AmbiusSC.utils.AmbiusConnection;

public class SiteDAO {

	private Connection conn;
	private CallableStatement cstmt = null;
	private ResultSet rs = null;
	private JSONArray ja = new JSONArray();
	private String data;
	
	public String getSiteData() throws ClassNotFoundException {
		
		try {
			conn = AmbiusConnection.getConnection();
			cstmt = conn.prepareCall("{call all_location_details()}");
			rs = cstmt.executeQuery();

			while (rs.next()) {
				JSONObject obj = new JSONObject();
				String id = rs.getString("service_locations_id");
				String client = rs.getString("client");
				String name = rs.getString("name");
				String description = rs.getString("description");
				String code = rs.getString("code");
				
				obj.put("id", id);
				obj.put("client", client);
				obj.put("name", name);
				obj.put("description", description);
				obj.put("code", code);				
				ja.add(obj);
			}

		} catch (SQLException se) {
			se.printStackTrace();
		}

		try {
			rs.close();
			cstmt.close();
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		data = ja.toString();
		return data;
	}
}
