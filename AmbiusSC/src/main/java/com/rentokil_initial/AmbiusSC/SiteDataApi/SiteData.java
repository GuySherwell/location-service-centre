package com.rentokil_initial.AmbiusSC.SiteDataApi;

	import java.io.IOException;

	import javax.servlet.annotation.WebServlet;
	import javax.servlet.http.HttpServlet;
	import javax.servlet.http.HttpServletRequest;
	import javax.servlet.http.HttpServletResponse;

import com.rentokil_initial.AmbiusSC.client.clientAPIs.ClientDAO;

	@WebServlet(name = "Site Details", urlPatterns = { "/site_details" })
	public class SiteData extends HttpServlet{
		@Override
		public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException{
			
		    String data = "No Data";
		    SiteDAO dao = new SiteDAO();
		    		
			try {
				data = dao.getSiteData();
				System.out.println(data);
			} catch (ClassNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			response.setContentType("application/json");
		    response.setCharacterEncoding("UTF-8");
		    response.getWriter().write(data);
			
		}
	}


