package com.rentokil_initial.AmbiusSC.site;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(
	    name = "Sites",
	    urlPatterns = {"/sites"}
	)
public class SiteServlet extends HttpServlet{
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException{
		
		List<Site> siteData = new ArrayList<Site>();
		
		String clientId = request.getParameter("clientId");
		String clientName = request.getParameter("clientName");
		System.out.println(clientId);
		try {
			SiteService ss = new SiteService();
			siteData = ss.getSiteData(clientId);
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		request.setAttribute("siteData", siteData);
		request.setAttribute("clientName", clientName);
		request.setAttribute("clientId", clientId);
		request.getRequestDispatcher("/WEB-INF/views/Sites.jsp").forward(request, response);
	}
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException{
		doGet(request, response);
	}
}
