package com.rentokil_initial.AmbiusSC.site;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.rentokil_initial.AmbiusSC.client.ClientService;

@WebServlet(
	    name = "Add Site",
	    urlPatterns = {"/addSite"}
	)
public class AddSiteServlet extends HttpServlet{

	private String client, clientName, name, description, code, serviceInterval, serviceStartDate, state;
	
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
		response.sendRedirect("/hello");
	}
	
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
		this.client = (String) request.getParameter("clientId");
		this.clientName = (String) request.getParameter("clientNam");
		this.name = (String) request.getParameter("name");
		this.description = (String) request.getParameter("description");
		this.code = (String) request.getParameter("code");
		this.serviceInterval = (String) request.getParameter("serviceInterval");
		this.serviceStartDate = (String) request.getParameter("serviceStartDate");
		
		SiteService ss = new SiteService();

		
		try {
			state = ss.addSiteData(client, name, description, code, serviceInterval, serviceStartDate);
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		}

				
		response.sendRedirect("/sites?clientId="+client+"&clientName="+clientName+"");
	}
	
}
