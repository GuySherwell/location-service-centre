package com.rentokil_initial.AmbiusSC.site;

public class Site {

	private String siteId, client, name, description, code, serviceInterval, serviceStartDate;
	
	public Site(String siteId, String client, String name, String description, String code, String serviceInterval, String serviceStartDate){
		this.siteId = siteId;
		this.client = client;
		this.name = name;
		this.description = description;
		this.code = code;
		this.serviceInterval = serviceInterval;
		this.serviceStartDate = serviceStartDate;
	}
	
	public Site(String siteId, String client, String name, String description, String code){
		this.siteId = siteId;
		this.client = client;
		this.name = name;
		this.description = description;
		this.code = code;
	}
	
	public String getSiteId() {
		return siteId;
	}

	public void setSiteId(String siteId) {
		this.siteId = siteId;
	}

	public String getClient() {
		return client;
	}

	public void setClient(String client) {
		this.client = client;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getServiceInterval() {
		return serviceInterval;
	}

	public void setServiceInterval(String serviceInterval) {
		this.serviceInterval = serviceInterval;
	}

	public String getServiceStartDate() {
		return serviceStartDate;
	}

	public void setServiceStartDate(String serviceStartDate) {
		this.serviceStartDate = serviceStartDate;
	}
}
