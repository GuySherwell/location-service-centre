package com.rentokil_initial.AmbiusSC.site;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(
	    name = "Delete Site",
	    urlPatterns = {"/deleteSite"}
	)
public class DeleteSiteServlet extends HttpServlet{
	
	private String clientId, clientName, siteId;
	
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
		response.sendRedirect("/hello");
	}
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException{
		
		this.clientId = (String) request.getParameter("deleteClientId");
		this.clientName = (String) request.getParameter("deleteClientName");
		this.siteId = (String) request.getParameter("deleteSiteId");
		
		SiteService ss = new SiteService();
		try {
			String status = ss.deleteSite(siteId);
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		}
		
		response.sendRedirect("/sites?clientId="+clientId+"&clientName="+clientName+"");
	}
}
