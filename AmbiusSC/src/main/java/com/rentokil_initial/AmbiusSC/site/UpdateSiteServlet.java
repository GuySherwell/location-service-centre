package com.rentokil_initial.AmbiusSC.site;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(
	    name = "Update Site",
	    urlPatterns = {"/updateSite"}
	)
public class UpdateSiteServlet extends HttpServlet{
	
	private String clientId, clientName, siteId, siteName, description, code, serviceInterval, serviceStartDate;
	
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
		response.sendRedirect("/hello");
	}
	
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
		this.clientId = (String) request.getParameter("editCientId");
		this.clientName = (String) request.getParameter("editClientNam");
		this.siteId = (String)request.getParameter("editSiteId");
		this.siteName = (String)request.getParameter("editName");
		this.description = (String)request.getParameter("editDescription");
		this.code = (String)request.getParameter("editCode");
		this.serviceInterval = (String)request.getParameter("editServiceInterval");
		this.serviceStartDate = (String)request.getParameter("editServiceStartDate");
		
		SiteService ss = new SiteService();
		try {
			String status = ss.updateSite(siteId, siteName, description, code, serviceInterval, serviceStartDate);
		} catch (ClassNotFoundException | SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		response.sendRedirect("/sites?clientId="+clientId+"&clientName="+clientName+"");
	}
}
	
