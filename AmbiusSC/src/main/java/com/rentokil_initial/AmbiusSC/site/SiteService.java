package com.rentokil_initial.AmbiusSC.site;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.rentokil_initial.AmbiusSC.utils.AmbiusConnection;
import com.rentokil_initial.AmbiusSC.utils.AmbiusConnectionLocal;
import com.rentokil_initial.AmbiusSC.utils.UpdateDbVersion;

public class SiteService {

	public List getSiteData(String clientId) throws ClassNotFoundException, SQLException {

		Connection conn = AmbiusConnection.getConnection();
		PreparedStatement stmt = null;
		ResultSet rs = null;
		List<Site> siteList = new ArrayList<Site>();

		String sql = "CALL select_sites(?);";
		stmt = conn.prepareStatement(sql);
		stmt.setString(1, clientId);
		rs = stmt.executeQuery();

		while (rs.next()) {
			String siteId = rs.getString("service_locations_id");
			String name = rs.getString("name");
			String description = rs.getString("description");
			String client = rs.getString("client");
			String code = rs.getString("code");
			String serviceInterval = rs.getString("service_interval");
			String serviceStartDate = rs.getString("service_start_date");

			Site site = new Site(siteId, client, name, description, code, serviceInterval, serviceStartDate);
			System.out.println(site.getServiceStartDate());
			siteList.add(site);
		}

		System.out.println("SQL call done with " + siteList.size() + " items");

		try {
			rs.close();
			stmt.close();
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return siteList;
	}

	// Add Site
	public String addSiteData(String client, String name, String description, String code, String serviceInterval,
			String serviceStartDate) throws ClassNotFoundException, SQLException {

		Connection conn = AmbiusConnection.getConnection();
		java.sql.PreparedStatement stmt = null;
		String state = "Unique Code Error";

		String sql = "CALL add_site(?, ?, ?, ?, ?, ?);";
		stmt = conn.prepareStatement(sql);
		stmt.setString(1, client);
		stmt.setString(2, name);
		stmt.setString(3, description);
		stmt.setString(4, code);
		stmt.setString(5, serviceInterval);
		stmt.setString(6, serviceStartDate);

		int i = stmt.executeUpdate();
		System.out.println("int value " + i);
		if (i == 1) {
			state = "Success";
			UpdateDbVersion udb = new UpdateDbVersion();
			udb.updateDB();
		}
		try {
			stmt.close();
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return state;
	}

	// Update Site
	public String updateSite(String siteId, String siteName, String description, String code, String serviceInterval,
			String serviceStartDate) throws ClassNotFoundException, SQLException {

		Connection conn = AmbiusConnection.getConnection();
		java.sql.PreparedStatement stmt = null;
		String state = "Unique Code Error";

		String sql = "CALL update_site(?, ?, ?, ?, ?, ?);";
		stmt = conn.prepareStatement(sql);
		stmt.setString(1, siteId);
		stmt.setString(2, siteName);
		stmt.setString(3, description);
		stmt.setString(4, code);
		stmt.setString(5, serviceInterval);
		stmt.setString(6, serviceStartDate);

		int i = stmt.executeUpdate();
		System.out.println("int value " + i);
		if (i == 1) {
			state = "Success";
			UpdateDbVersion udb = new UpdateDbVersion();
			udb.updateDB();
		}
		try {
			stmt.close();
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return state;
	}

	// Delete Site
	public String deleteSite(String siteId) throws ClassNotFoundException, SQLException {

		Connection conn = AmbiusConnection.getConnection();
		java.sql.PreparedStatement stmt = null;
		String state = "Unique Code Error";

		String sql = "CALL delete_site(?);";
		stmt = conn.prepareStatement(sql);
		stmt.setString(1, siteId);

		int i = stmt.executeUpdate();
		System.out.println("int value " + i);
		if (i == 1) {
			state = "Success";
			UpdateDbVersion udb = new UpdateDbVersion();
			udb.updateDB();
		}
		try {
			stmt.close();
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return state;
	}
}
