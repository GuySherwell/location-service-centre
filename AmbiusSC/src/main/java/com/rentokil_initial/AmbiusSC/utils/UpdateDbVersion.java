package com.rentokil_initial.AmbiusSC.utils;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class UpdateDbVersion {

	private Connection conn;
	private PreparedStatement stmt = null;

	public void updateDB() throws ClassNotFoundException {

		try {
			conn = AmbiusConnection.getConnection();
			String sql = "CALL update_db_version();";
			stmt = conn.prepareStatement(sql);
			int i = stmt.executeUpdate();
		} catch (SQLException e1) {
			e1.printStackTrace();
		}

		try {
			stmt.close();
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
}
