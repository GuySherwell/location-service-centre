package com.rentokil_initial.AmbiusSC.utils;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class AmbiusConnection {

	// JDBC driver name and database URL
	static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";
	static final String DB_URL = "/ambius_service_center?";

	String instanceName = "";

	// Database credentials
	static final String USER = "";
	static final String PASS = "";
	
	public static Connection getConnection() throws SQLException, ClassNotFoundException{
		
		Connection conn = null;
		
		Class.forName("com.mysql.jdbc.GoogleDriver");
		
		System.out.println("Connecting to database...");
		conn = DriverManager.getConnection(DB_URL);
		
		return conn;
		
	}
}
