package com.rentokil_initial.AmbiusSC.APIs.ScanDataApi;

import java.io.IOException;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.rentokil_initial.AmbiusSC.SiteDataApi.SiteDAO;

@WebServlet(name = "Scan Details", urlPatterns = { "/scan_details" })
public class ScanData extends HttpServlet{
	
	@Override
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException{
		
		String locationId = request.getParameter("locationId");
		String clientId = request.getParameter("clientId");
		String date = request.getParameter("date");
		String email = request.getParameter("email");
		String notes = request.getParameter("notes");
		String status = request.getParameter("status");
		String user = request.getParameter("user");
	    String data = "No Data";
	    
	    ScanDAO dao = new ScanDAO();
	    		
		try {
			data = dao.setScanData(date, locationId, clientId, notes, email, status, user);
			System.out.println(data);
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		
		response.setContentType("application/json");
	    response.setCharacterEncoding("UTF-8");
	    response.getWriter().write(data);
	}
}
