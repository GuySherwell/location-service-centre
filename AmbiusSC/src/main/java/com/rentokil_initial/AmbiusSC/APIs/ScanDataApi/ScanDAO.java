package com.rentokil_initial.AmbiusSC.APIs.ScanDataApi;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Calendar;
import java.util.Date;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import com.rentokil_initial.AmbiusSC.utils.AmbiusConnection;

public class ScanDAO {

	private String date, locationId, clientId, notes, email, status;
	private Connection conn;
	private CallableStatement cstmt = null;
	private ResultSet rs = null;
	private JSONArray ja = new JSONArray();
	private String data;

	public String setScanData(String date, String locationId, String clientId, String notes, String email, String status, String user) throws ClassNotFoundException {

		this.date = date;
		this.locationId = locationId;
		this.clientId = clientId;
		this.notes = notes;
		this.email = email;
		this.status = status;
		
		try {
			conn = AmbiusConnection.getConnection();
			cstmt = conn.prepareCall("{call scan_details(?, ?, ?, ?, ?, ?, ?)}");
			cstmt.setString(1, date);
			cstmt.setString(2, locationId);
			cstmt.setString(3, clientId);
			cstmt.setString(4, notes);
			cstmt.setString(5, email);
			cstmt.setString(6, status);
			cstmt.setString(7, user);
			cstmt.executeUpdate();

			JSONObject obj = new JSONObject();
			obj.put("location", locationId);
			obj.put("status", "Success");
			ja.add(obj);
		} catch (SQLException se) {	
			JSONObject obj = new JSONObject();
			obj.put("location", locationId);
			obj.put("status", "Failed");
			ja.add(obj);
			se.printStackTrace();
		}
		try {
			cstmt.close();
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		data = ja.toString();
		return data;
	}
}
