package com.rentokil_initial.AmbiusSC.APIs.VersionDataApi;

import java.io.IOException;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.rentokil_initial.AmbiusSC.SiteDataApi.SiteDAO;

@WebServlet(name = "Version Details", urlPatterns = { "/version_details" })
public class VersionData extends HttpServlet{
	
	@Override
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException{
		
	    String data = "No Data";
	    VersionDAO dao = new VersionDAO();
	    		
		try {
			data = dao.getVersionData();
			System.out.println(data);
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		
		response.setContentType("application/json");
	    response.setCharacterEncoding("UTF-8");
	    response.getWriter().write(data);
		
	}
}
