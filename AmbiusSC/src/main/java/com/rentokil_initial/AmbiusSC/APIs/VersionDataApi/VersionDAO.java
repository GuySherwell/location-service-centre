package com.rentokil_initial.AmbiusSC.APIs.VersionDataApi;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Calendar;
import java.util.Date;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import com.rentokil_initial.AmbiusSC.utils.AmbiusConnection;

public class VersionDAO {

	private Connection conn;
	private CallableStatement cstmt = null;
	private ResultSet rs = null;
	private JSONArray ja = new JSONArray();
	private String data;
	
	public String getVersionData() throws ClassNotFoundException {

		try {
			conn = AmbiusConnection.getConnection();
			String sql = "{call db_version_details()}";
			cstmt = conn.prepareCall(sql);
			rs = cstmt.executeQuery();
			
			while(rs.next()){
				JSONObject obj = new JSONObject();
				String version = rs.getString("version");
				obj.put("version", version);
				ja.add(obj);
			}

		} catch (SQLException se) {
			JSONObject obj = new JSONObject();
			obj.put("status", "Failed");
			ja.add(obj);
			se.printStackTrace();
		}

		try {
			cstmt.close();
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		data = ja.toString();
		
		return data;
	}
}
