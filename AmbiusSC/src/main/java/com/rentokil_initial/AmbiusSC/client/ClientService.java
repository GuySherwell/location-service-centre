package com.rentokil_initial.AmbiusSC.client;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.rentokil_initial.AmbiusSC.utils.AmbiusConnection;
import com.rentokil_initial.AmbiusSC.utils.AmbiusConnectionLocal;
import com.rentokil_initial.AmbiusSC.utils.UpdateDbVersion;

public class ClientService {

	public List getClientData() throws ClassNotFoundException, SQLException {

		Connection conn = AmbiusConnection.getConnection();
		Statement stmt = null;
		ResultSet rs = null;
		List<Client> clientList = new ArrayList<Client>();

		stmt = conn.createStatement();
		String sql;
		sql = "CALL select_all();";
		rs = stmt.executeQuery(sql);

		while (rs.next()) {
			String id = rs.getString("idclients");
			String name = rs.getString("name");
			String address = rs.getString("address");
			Client client = new Client(id, name, address);
			clientList.add(client);
		}
		
		try {
			rs.close();
			stmt.close();
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return clientList;
	}

	public void addClientData(String name, String address) throws ClassNotFoundException, SQLException {

		Connection conn = AmbiusConnection.getConnection();
		java.sql.PreparedStatement stmt = null;

		String sql = "CALL add_client(?, ?);";
		stmt = conn.prepareStatement(sql);
		stmt.setString(1, name);
		stmt.setString(2, address);

		int i = stmt.executeUpdate();
		System.out.println("int value " + i);
		if(i == 1){
			UpdateDbVersion udb = new UpdateDbVersion();
			udb.updateDB();
		}

		try {
			stmt.close();
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
}
