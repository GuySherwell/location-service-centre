package com.rentokil_initial.AmbiusSC.client;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONArray;

@WebServlet(
	    name = "Clients",
	    urlPatterns = {"/clients"}
	)
public class ClientServlet extends HttpServlet{ 
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException{
		
		ClientService data = new ClientService();
		
		try {
			request.removeAttribute("ClientData");
			List<Client> clientData = new ArrayList<Client>();
			clientData = data.getClientData();
			System.out.println(clientData.size());
			request.setAttribute("ClientData", clientData);
			
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		request.getRequestDispatcher("/WEB-INF/views/Clients.jsp").forward(request, response);
	}
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException{
		
		response.sendRedirect("/udemycourse/todo.do");
	}
}
