package com.rentokil_initial.AmbiusSC.client.clientAPIs;

import java.io.IOException;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(name = "Test", urlPatterns = { "/get_details" })
public class ClientData extends HttpServlet{
	@Override
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException{
		
	    String data = "No Data";
	    ClientDAO dao = new ClientDAO();
	    		
		try {
			data = dao.getClientData();
			System.out.println(data);
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		
		response.setContentType("application/json");
	    response.setCharacterEncoding("UTF-8");
	    response.getWriter().write(data);
	}
}
