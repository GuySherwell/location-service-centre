package com.rentokil_initial.AmbiusSC.client;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(
	    name = "Add Clients",
	    urlPatterns = {"/addClient"}
	)
public class AddClientServlet extends HttpServlet{

	private String name;
	private String address;
	
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
		this.name = (String) request.getParameter("name");
		
		System.out.println(name);
		this.address = (String) request.getParameter("address");
		System.out.println(address);
		ClientService cs = new ClientService();

		try {
			cs.addClientData(name, address);
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}

		response.sendRedirect("/clients");
	}
}
