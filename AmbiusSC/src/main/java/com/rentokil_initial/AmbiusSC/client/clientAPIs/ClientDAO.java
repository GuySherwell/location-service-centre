package com.rentokil_initial.AmbiusSC.client.clientAPIs;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import com.rentokil_initial.AmbiusSC.utils.AmbiusConnection;

public class ClientDAO {

	private Connection conn;
	private Statement stmt = null;
	private ResultSet rs = null;
	private JSONArray ja = new JSONArray();
	private String data;
	
	public String getClientData() throws ClassNotFoundException {
		
		try {

			conn = AmbiusConnection.getConnection();
			stmt = conn.createStatement();
			String sql;
			sql = "CALL select_all();";
			rs = stmt.executeQuery(sql);

			while (rs.next()) {
				JSONObject obj = new JSONObject();
				String id = rs.getString("idclients");
				String name = rs.getString("name");
				String address = rs.getString("address");
				obj.put("id", id);
				obj.put("name", name);
				obj.put("address", address);
				ja.add(obj);
			}

		} catch (SQLException se) {
			se.printStackTrace();
		}
		System.out.println("SQL call done");

		try {
			rs.close();
			stmt.close();
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		data = ja.toString();
		return data;
	}
}